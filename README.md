# asdf-polylith

[Polylith](https://polylith.gitbook.io/polylith/) plugin for
[asdf](https://github.com/asdf-vm/asdf) version manager.

## Install ##

    asdf plugin-add polylith https://gitlab.com/runswithd6s/asdf-polylith.git

## Requirements ##

Java and Clojure are required for Polylith, and they can also be installed with
respective asdf-vm plugins.

  - [halcyon/asdf-java](https://github.com/halcyon/asdf-java)
  - [halcyon/asdf-clojure](https://github.com/halcyon/asdf-clojure)

## Use ##

Check [asdf](https://github.com/asdf-vm/asdf) readme for instructions
on how to install & manage versions of Polylith.
